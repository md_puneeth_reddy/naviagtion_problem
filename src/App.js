import React from "react";
import Login from "./components/Login";
import { Router } from "@reach/router";
import "./App.css";
import Signup from "./components/Signup";
import Home from "./components/Home";

function App() {
  return (
    <div className="App">
      <Home />
      <Router>
        <Login path="/Login"></Login>
        <Signup path="/Signup"></Signup>
      </Router>
    </div>
  );
}

export default App;
