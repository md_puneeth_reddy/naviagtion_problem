import React, { useState } from "react";
import { navigate } from "@reach/router";
const Home = () => {
  return (
    <div>
      <button onClick={() => navigate("./Login")}>Login</button>
      <button onClick={() => navigate("./Signup")}>Signup</button>
    </div>
  );
};

export default Home;
